#!/bin/bash
set -x

go get -d ./...
golangci-lint run
