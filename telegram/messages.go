// Copyright 2022 Galanov Aleksandr
// Licensed under the Apache License, Version 2.0

package telegram

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"os"
)

type StikcerSending struct {
	ChatID           int64  `json:"chat_id"`
	Sticker          string `json:"sticker"`
	ReplyToMessageID int64  `json:"reply_to_message_id"`
}

type MessageSending struct {
	ChatID           int64        `json:"chat_id"`
	Text             string       `json:"text"`
	ReplyToMessageID int64        `json:"reply_to_message_id,omitempty"`
	ReplyMarkup      *ReplyMarkup `json:"reply_markup,omitempty"`
}

type MessageDeletion struct {
	ChatID    int64 `json:"chat_id"`
	MessageID int64 `json:"message_id"`
}

type BanChatMember struct {
	ChatID         int64 `json:"chat_id"`
	UserID         int64 `json:"user_id"`
	UntilUnixDate  int64 `json:"until_date,omitempty"`
	RevokeMessages bool  `json:"revoke_messages,omitempty"`
}

type UnbanChatMember struct {
	ChatID int64 `json:"chat_id"`
	UserID int64 `json:"user_id"`
}

type PoolSending struct {
	ChatID                   int64            `json:"chat_id"`
	Question                 string           `json:"question"`
	Options                  []string         `json:"options"`
	IsAnonymous              bool             `json:"is_anonymous"`
	PollType                 string           `json:"type"` // “regular” or “quiz”
	AllowsMultipleAnswers    bool             `json:"allows_multiple_answers"`
	CorrectOptionID          int              `json:"correct_option_id"`
	Explanation              string           `json:"explanation"`
	ExplanationParseMode     string           `json:"explanation_parse_mode"`
	ExplanationEntities      []*MessageEntity `json:"explanation_entities"`
	OpenPeriod               int64            `json:"open_period"`
	CloseDate                int64            `json:"close_date"`
	IsClosed                 bool             `json:"is_closed"`
	DisableNotification      bool             `json:"disable_notification"`
	ProtectContent           bool             `json:"protect_content"`
	ReplyToMessageID         int64            `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool             `json:"allow_sending_without_reply"`
}

type VoiceSending struct {
	ChatID                   int64  `json:"chat_id"`
	Voice                    string `json:"voice"`
	ProtectContent           bool   `json:"protect_content"`
	ReplyToMessageID         int64  `json:"reply_to_message_id"`
	AllowSendingWithoutReply bool   `json:"allow_sending_without_reply"`
}

type ChatAsking struct {
	ChatID int64 `json:"chat_id"`
}

type PinSending struct {
	ChatID              int64 `json:"chat_id"`
	MessageID           int64 `json:"message_id"`
	DisableNotification bool  `json:"disable_notification"`
}

type UnpinSending struct {
	ChatID    int64 `json:"chat_id"`
	MessageID int64 `json:"message_id"`
}

type UpdateRequest struct {
	Offset  int `json:"offset"`
	Limit   int `json:"limit"`
	Timeout int `json:"timeout"`
}

type RequestResult struct {
	Ok     bool `json:"ok"`
	Result bool `json:"result"`
}

type IntResult struct {
	Ok     bool `json:"ok"`
	Result int  `json:"result"`
}

type Updates struct {
	Status  bool      `json:"ok"`
	Updates []*Update `json:"result"`
}

type Update struct {
	ID                int            `json:"update_id"`
	Message           *Message       `json:"message"`
	EditedMessage     *Message       `json:"edited_message"`
	ChannelPost       *Message       `json:"channel_post"`
	EditedChannelPost *Message       `json:"edited_channel_post"`
	CallbackQuery     *CallbackQuery `json:"callback_query"`
}

type CallbackQuery struct {
	ID              int64    `json:"id"`
	From            *User    `json:"from"`
	Message         *Message `json:"message"`
	InlineMessageID string   `json:"inline_message_id"`
	ChatInstance    string   `json:"chat_instance"`
	Data            string   `json:"data"`
	GameShortName   string   `json:"game_short_name"`
}

type Message struct {
	ID                   int64            `json:"message_id"`
	From                 *User            `json:"from"`
	SenderChat           *Chat            `json:"sender_chat"`
	Date                 int64            `json:"date"`
	Chat                 *Chat            `json:"chat"`
	ForwardFrom          *User            `json:"forward_from"`
	ForwardFromChat      *Chat            `json:"forward_from_chat"`
	ForwardFromMessageID int64            `json:"forward_from_message_id"`
	ForwardSignature     string           `json:"forward_signature"`
	ForwardSenderName    string           `json:"forward_sender_name"`
	ForwardDate          int64            `json:"forward_date"`
	IsAutomaticForward   bool             `json:"is_automatic_forward"`
	ReplyToMessage       *Message         `json:"reply_to_message"`
	ViaBot               *User            `json:"via_bot"`
	EditDate             int64            `json:"edit_date"`
	HasProtectedContent  bool             `json:"has_protected_content"`
	MediaGroupID         string           `json:"media_group_id"`
	AuthorSignature      string           `json:"author_signature"`
	Text                 string           `json:"text"`
	Sticker              *Stiker          `json:"sticker"`
	Poll                 *Poll            `json:"poll"`
	PollAnswer           *PollAnswer      `json:"poll_answer"`
	Photo                []*Photo         `json:"photo"`
	Caption              string           `json:"caption"`
	Entities             []*MessageEntity `json:"entities"`
}

type ReplyMarkup struct {
	InlineKeyboard [][]*InlineKeyboardButton `json:"inline_keyboard"`
}

type InlineKeyboardButton struct {
	Text         string `json:"text"`
	CallbackData string `json:"callback_data"`
}

type Stiker struct {
	FileID       string `json:"file_id"`
	FileUniqueID string `json:"file_unique_id"`
}

type MessageEntity struct {
	EntityType string `json:"type"`
	Offset     int    `json:"offset"`
	Length     int    `json:"length"`
	URL        string `json:"url"`
	User       *User  `json:"user"`
	Language   string `json:"language"`
}

type Poll struct {
	ID                    int64            `json:"id"`
	Question              string           `json:"question"`
	Options               []*PollOption    `json:"options"`
	TotalVoterCount       int              `json:"total_voter_count"`
	IsClosed              bool             `json:"is_closed"`
	IsAnonymous           bool             `json:"is_anonymous"`
	PollType              string           `json:"type"` // “regular” or “quiz”
	AllowsMultipleAnswers bool             `json:"allows_multiple_answers"`
	CorrectOptionID       int              `json:"correct_option_id"`
	Explanation           string           `json:"explanation"`
	ExplanationEntities   []*MessageEntity `json:"explanation_entities"`
	OpenPeriod            int64            `json:"open_period"`
	CloseDate             int64            `json:"close_date"`
}

type PollAnswer struct {
	PollID    int64 `json:"poll_id"`
	User      *User `json:"user"`
	OptionIDs []int `json:"option_ids"`
}

type PollOption struct {
	Text       string `json:"text"`
	VoterCount int    `json:"voter_count"`
}

type Photo struct {
	FileID       string `json:"file_id"`
	FileUniqueID string `json:"file_unique_id"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
	FileSize     int    `json:"file_size"`
}

type Chat struct {
	ID       int64  `json:"id"`
	ChatType string `json:"type"`
	Username string `json:"username"`
}

type User struct {
	ID           int64  `json:"id"`
	IsBot        bool   `json:"is_bot"`
	FirstName    string `json:"first_name"`
	Username     string `json:"username"`
	LanguageCode string `json:"language_code"`
}

type UserPermission struct {
	SendMessage bool `json:"can_send_messages"`
	SendMedia   bool `json:"can_send_media_messages"`
	SendPool    bool `json:"can_send_polls"`
	SendStikers bool `json:"can_send_other_messages"`
	AddBebPage  bool `json:"can_add_web_page_previews"`
	ChangeInfo  bool `json:"can_change_info"`
	InviteUser  bool `json:"can_invite_users"`
	PinMessage  bool `json:"can_pin_messages"`
}

type SettingUserPermission struct {
	ChatID      int64           `json:"chat_id"`
	UserID      int64           `json:"user_id"`
	Permissions *UserPermission `json:"permissions"`
}

var errToken = errors.New("can't find token")

func AskForUpdates(offset int) (*Updates, error) {
	requestBody := UpdateRequest{}
	requestBody.Offset = offset + 1
	requestBody.Limit = 200
	requestBodyText, err := json.Marshal(requestBody)
	if err != nil {
		return nil, err
	}
	result, err := sendAPIRequest("getUpdates", requestBodyText)
	var updates Updates
	if err != nil {
		return &updates, err
	}
	err = json.Unmarshal(result, &updates)

	return &updates, err
}

func (sticker StikcerSending) Send() (*Message, error) {
	bodyText, err := json.Marshal(sticker)
	if err != nil {
		return nil, err
	}

	return sendEntity("sendSticker", bodyText)
}

func (message MessageSending) Send() (*Message, error) {
	bodyText, err := json.Marshal(message)
	if err != nil {
		return nil, err
	}

	return sendEntity("sendMessage", bodyText)
}

func (poll PoolSending) Send() (*Message, error) {
	bodyText, err := json.Marshal(poll)
	if err != nil {
		return nil, err
	}

	return sendEntity("sendPoll", bodyText)
}

func (voice VoiceSending) Send() (*Message, error) {
	bodyText, err := json.Marshal(voice)
	if err != nil {
		return nil, err
	}

	return sendEntity("sendVoice", bodyText)
}

func (pin PinSending) Send() (bool, error) {
	bodyText, err := json.Marshal(pin)
	if err != nil {
		return false, err
	}
	resultBytes, err := sendAPIRequest("pinChatMessage", bodyText)
	if err != nil {
		return false, err
	}
	result := RequestResult{}
	err = json.Unmarshal(resultBytes, &result)

	return result.Result, err
}

func (unpin UnpinSending) Send() (bool, error) {
	bodyText, err := json.Marshal(unpin)
	if err != nil {
		return false, err
	}
	resultBytes, err := sendAPIRequest("unpinChatMessage", bodyText)
	if err != nil {
		return false, err
	}
	result := RequestResult{}
	err = json.Unmarshal(resultBytes, &result)

	return result.Result, err
}

func (chat ChatAsking) GetChat() (*Chat, error) {
	bodyText, err := json.Marshal(chat)
	if err != nil {
		return nil, err
	}
	resultBytes, err := sendAPIRequest("getChat", bodyText)
	if err != nil {
		return nil, err
	}
	result := Chat{}
	err = json.Unmarshal(resultBytes, &result)

	return &result, err
}

func (chat ChatAsking) GetChatMembers() (int, error) {
	bodyText, err := json.Marshal(chat)
	if err != nil {
		return 0, err
	}
	resultBytes, err := sendAPIRequest("getChatMemberCount", bodyText)
	if err != nil {
		return 0, err
	}
	result := IntResult{}
	err = json.Unmarshal(resultBytes, &result)

	return result.Result, err
}

func (message Message) Delete() (int, error) {
	deletion := MessageDeletion{}
	deletion.ChatID = message.Chat.ID
	deletion.MessageID = message.ID
	bodyText, err := json.Marshal(deletion)
	if err != nil {
		return 0, err
	}
	resultBytes, err := sendAPIRequest("deleteMessage", bodyText)
	if err != nil {
		return 0, err
	}
	result := IntResult{}
	err = json.Unmarshal(resultBytes, &result)

	return result.Result, err
}

func (message Message) BanUser(unixTill int64, revokeMessages bool) error {
	ban := BanChatMember{
		ChatID:         message.Chat.ID,
		UserID:         message.From.ID,
		RevokeMessages: revokeMessages,
	}
	if unixTill != 0 {
		ban.UntilUnixDate = unixTill
	}

	bodyText, err := json.Marshal(ban)
	if err != nil {
		return err
	}
	_, err = sendAPIRequest("banChatMember", bodyText)

	return err
}

func (unbanChatMember UnbanChatMember) Unban() error {
	bodyText, err := json.Marshal(unbanChatMember)
	if err != nil {
		return err
	}
	_, err = sendAPIRequest("banChatMember", bodyText)

	return err
}

func (permissions SettingUserPermission) Set() error {
	bodyText, err := json.Marshal(permissions)
	if err != nil {
		return err
	}
	_, err = sendAPIRequest("RestrictChatMember", bodyText)

	return err
}

func sendEntity(method string, body []byte) (*Message, error) {
	messageBytes, err := sendAPIRequest(method, body)
	if err != nil {
		return nil, err
	}
	message := Message{}
	err = json.Unmarshal(messageBytes, &message)

	return &message, err
}

func sendAPIRequest(method string, bodyRequest []byte) ([]byte, error) {
	token := os.Getenv("BOT_TELEGRAM_BOT_TOKEN")
	if token == "" {
		return nil, errToken
	}
	URL := "https://api.telegram.org/bot" + token + "/" + method
	req, err := http.NewRequestWithContext(context.Background(), http.MethodPost, URL, bytes.NewBuffer(bodyRequest))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	bodyResponse, err := io.ReadAll(resp.Body)

	return bodyResponse, err
}
